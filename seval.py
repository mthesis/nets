import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model


import sys

from sep import getmodel, base



f=np.load("predicted.npz")
q1,q2,q3=f["q"],f["q2"],f["q3"]
del f

mul=int(q1.shape[-1])

#y=keras.utils.to_categorical(y,2)

index=None
try:
  if len(sys.argv)>1:index=int(sys.argv[1])
except:pass

id=1
if len(sys.argv)>2:id=int(sys.argv[2])


print(f"Working on {index}  {id}")

#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

mis=["b"]#,"a"]

#if len(sys.argv)>1:
#  cut=int(sys.argv[1])



#  mis=[["b","a"][cut]]

for mi in mis:
  model=getmodel(list([ac*mul for ac in base]))
  if type(index)==type(None):
    model.load_weights(f"msep{id}.tf")
  else:
    model.load_weights(f"multi/{index}/msep{id}.tf")

  model.summary()





  my1=model.predict(q1,verbose=1)
  my2=model.predict(q2,verbose=1)
  my3=model.predict(q3,verbose=1)
  if type(index)==type(None):
    np.savez_compressed(f"seval{id}",q1=my1,q2=my2,q3=my3)
  else:
    np.savez_compressed("multi/"+str(index)+f"/seval{id}",x=x,y=y,p=p,s=my)



