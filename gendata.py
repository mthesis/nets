import numpy as np

from numpy.random import randint as rndi
from numpy.random import normal

import matplotlib.pyplot as plt

def randomperson():
  ret= np.array(
    [1,#flag
    rndi(1,4),#age
    rndi(0,2),#gender
    0.0])
  ret[3]=(2**ret[1])*normal(1,0.1,1)/16.0+normal(0,1,1)#wealth

  return ret

def persons(n=1000):
  ret=[]
  for i in range(n):
    ret.append(randomperson())
  return np.array(ret)

def similarity(p1,p2,i,j):
  if i==j:return 0
  ret=1.0
  ret*=np.exp(-np.sum((p1-p2)**2))
  ret*=np.exp(-0.1*np.abs(i-j))
  return ret

def guessprob(A,k=5,base=None):
  print("guess probing")
  if base is None:
    ret=np.zeros_like(A)
  else:
    ret=base
  la=len(A)
  for i in range(k*len(A)):
    i1=rndi(len(A))
    s=np.sum(A[i1])
    r=np.random.random()*s
    for i2 in range(len(A)):
      r-=A[i1][i2]
      if r<0:break
    ret[i1,i2]=1
    ret[i2,i1]=1
    print("gp",i/(k*la))
  return ret
    
    

def relperpos(q,k=5):
  lq=len(q)
  pro=np.zeros((lq,lq))
  print("similaring")
  for i in range(lq):
    for j in range(lq):
      #if (i<lq-100 and i>=lq+100) or (j<lq-100 and j>=lq+100):#generate random connections
      #  pro[i,j]=1.0
      #  continue
      pro[i,j]=similarity(q[i],q[j],i,j)
    print("sp",i/lq)
  
  ret=guessprob(pro,k=k-1,base=None)
  
  
  #last 100 lowcon
  
  ret[:,-100:]=0
  ret[-100:,:]=0
  
  ret=guessprob(pro,k=1,base=ret)
  
  
  return ret

def neiglist1d(A):
  ret=[]
  for i,aa in enumerate(A):
    if aa>0.5:
      ret.append(i)
  return ret

def neiglist(A):
  ret=[]
  for i in range(len(A)):
    ret.append(neiglist1d(A[i]))
  return ret

def expandlis(n):
  ret=[]
  for i in range(len(n)):
    # nn=set(list(np.array([n[w] for w in n[i]]).flatten()))
    nn=[]
    for w in n[i]:
      nn.append(w)
      for ww in n[w]:
        nn.append(ww)
    
    nn=list(set(nn))
    
    ret.append(nn)
  return ret
  
def partofmat(A,l,l0):
  ll=[l0]
  for al in l:
    if al!=l0:ll.append(al)
  r1=[]
  for lx in l:
    r1.append(A[lx])
  r1=np.array(r1)
  r2=[]
  for lx in l:
    r2.append(r1[:,lx])
  return np.array(r2)

def partofvec(q,n,n0):
  nn=[n0]
  for an in n:
    if an!=n0:nn.append(an)
  return q[nn]

def nosc(A):
  A[1:,1:]=0
  A[1:,0]=1
  A[0,1:]=1
  A[0,0]=1
  return A

def selffilt(q):
  ret=[]
  for i,qq in enumerate(q):
    if i in qq:qq.remove(i)
    ret.append(qq)
  return ret

def partulate(A,q,n):
  rA,rq=[],[]

  ln=len(n) 
 
  for i,nn in enumerate(n):
    acA=partofmat(A,nn,i)
    acq=partofvec(q,nn,i)
 
    #if i<ln-100 and i>=ln-200:
    #  acA=nosc(acA)

    rA.append(acA)
    rq.append(acq)
  
  return rA,rq


np.random.seed(12)

print("generating")

q=persons(n=5000)
#q=persons(n=500)
print("generated persons")

A=relperpos(q,k=5)
print("generated Matrix")

# plt.plot(np.sum(A,axis=0))
# plt.show()
# exit()


n1=neiglist(A)
print("gen neig 1")
n2=expandlis(n1)
print("gen neig 2")

n2=selffilt(n2)
print("selffilt")


mat,vec=partulate(A,q,n2)
print("partulate")

print(mat[0].shape)


np.savez_compressed("data.npz",mat=mat,vec=vec)
print("saved and done")

exit()


print(mat[0])
print(vec[0])

exit()


print(partofmat(A,n2[0]))
print(partofvec(q,n2[0]))

exit()


# print(n2)
ll=[len(n) for n in n2]




plt.hist(ll,bins=20)

plt.show()












