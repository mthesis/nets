import numpy as np
import matplotlib.pyplot as plt

import sys
fix=True
if len(sys.argv)>1:
  fix=False



f=np.load("eval.npz")


def lbyn(n=""):


  x,p=f["x"+n],f["p"+n]


  x=x[:,:,:p.shape[-1]]


  l=(x-p)**2

  l=np.mean(l,axis=(1,2))
  l=np.sqrt(l)

  return l


l=lbyn()
l2=lbyn("2")
l3=lbyn("3")


c=0

#for ll,qq in zip(ln,qn):
#  if ll>0.1:
#    c+=1
#    print(qq,ll)
#  else:
#    print("      ",qq,ll)

#print(c,len(ln),c/len(ln))
#print("maxima",qn[np.argmax(ln)])
#print("minima",qn[np.argmin(ln)])

plt.hist(l,bins=20,alpha=0.5,label="training")
#plt.hist(l2,bins=20,alpha=0.5,label="2")
plt.hist(l3,bins=20,alpha=0.5,label="anomalies")


plt.xlabel("loss")
plt.ylabel("Count")

plt.yscale("log",nonposy="clip")

plt.legend()

plt.savefig("imgs/histlossNETS.png",format="png")
plt.savefig("imgs/histlossNETS.pdf",format="pdf")


plt.show()


