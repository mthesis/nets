import numpy as np
import matplotlib.pyplot as plt


f=np.load("seval1.npz")

q1,q2,q3=f["q1"],f["q2"],f["q3"]

m=np.mean(q1)

def fix(q):
  return np.abs(np.mean(q,axis=-1)-m)


q1=fix(q1)
q2=fix(q2)
q3=fix(q3)






plt.hist(q1,bins=50,alpha=0.5,label="training",density=True)
#plt.hist(q2,bins=20,alpha=0.5,label="q2",density=True)
plt.hist(q3,bins=50,alpha=0.5,label="anomaly",density=True)



plt.legend()
plt.yscale("log",nonposy="clip")


plt.savefig("imgs/oohistNETS.png",format="png")
plt.savefig("imgs/oohistNETS.pdf",format="pdf")


plt.show()




