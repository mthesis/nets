import numpy as np
import matplotlib.pyplot as plt


f=np.load("data.npz",allow_pickle=True)

x=f["vec"]
a=f["mat"]

typ=np.ones(len(x))
typ[-200:-100]=2
typ[-100:]=3



print(a[0].shape)
print(a[-150].shape)
print(a[-50].shape)
#exit()


l=[len(ax) for ax in x]


s=np.max(l)

# s=30
s=70

#print(x[0].shape)
#print(np.zeros((s-len(x[0]),x[)))



xx=np.array([np.concatenate((ax,np.zeros((s-len(ax),np.array(ax).shape[-1]))),axis=0) for ax in x if len(ax)<=s])


print(xx.shape)


print(a[0].shape)
print(s-len(a[0]),len(a[0]))


aq=[]
ax=[]
print("start")

at=[]



for aa,xxx,tt in zip(a,xx,typ):
    if tt==2:continue
    if tt==3:tt=2
    #try:
    for i in range(1):
        la=len(aa)
        if la>s:continue
        if la<1:continue
        ap=np.zeros((s-la,la))
        #print(aa.shape)
        #print(la)
        #print(ap.shape)
        #print(s)
        #exit()
        aa=np.concatenate((aa,ap),axis=0)
        ap=np.zeros((len(aa),s-la))
        aa=np.concatenate((aa,ap),axis=1)
        aq.append(aa)
        ax.append(xxx)   
        at.append(tt)


    #except:
    #    print("failed at",aa.shape)
aq=np.array(aq)
ax=np.array(ax)
at=np.array(at)

print(aq.shape,ax.shape,at.shape)

print("!",len([t for t in at if t==2]))
print("!",len([t for t in at if t==3]))

print(aq.shape)

print("fraction",len(aq)/len(a),len(aq))


np.savez_compressed("data_1.npz",a=aq,x=ax,t=at)
